<?php

return [
    'welcome' => 'Welcome to our website.',
    'description' =>'Now we still have a lot of information about this. But as Wikileaks has obtained a screenshot of an email Mossack Fonseca (the source of these documents) sent to the client to interpret the information out. In an email explaining the problem. "Hacked mail servers."',
    'home' => 'Home',
    'newsandevent'=>'News & Event',
    'drinks' => 'Drinks',
    'video' => 'Videos',
    'standard' => 'Standard',
    'contact' => 'Contact',
    '1688' => '1688 company World Wide Ltd.',
    '13/47' => '13/47 Moo 7 Navamin Klongkum BuengKumDistrict,Bangkok 10230.',
    'phone' => 'Phone',
    'fax' => 'Fax',
    'worker' => 'Worker',
    'sports' => 'Sports',
    'agriculture' => 'Egriculture',
    'education' => 'Education',
    'videos' => 'Videos',
    'map' => 'Map',
    'fillContact' => 'Please fill in the fields below to complete convenience. We will get back to you as soon as the end.',

    'benefit' => 'Benefit',
    'convenient' => 'Convenient',
    'share' => 'Share',
    'by' => 'By',

    'sucrose' => 'Sucrose',
    'b3' => 'Vitamin B3',
    'b5' => 'Vitamin B5',
    'b6' => 'Vitamin B6',
    'dexpanthenol' => 'Dexpanthenol',
    'b12' => 'Vitamin B12',
    'taurine' => 'Taurine',
    'caffeine' => 'caffeine',
    'Inositol' => 'Inositol',
    'vitaminC' => 'Vitamin C',
    'fiber' => 'Fiber',
    'ingredient' => 'Ingredient',
    'sugar' => 'Sugar',
    'artificial' => 'Natural color Artificial Flavor Preservatives'

];