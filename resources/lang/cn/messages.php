<?php

return [
    'welcome' => '欢迎光临我们的网站。',
    'description' => '现在，我们仍然有很多这方面的信息。但如维基解密已获得发送到客户端解释的信息一封电子邮件Mossack塞卡（这些文件的来源）的屏幕截图。在一封电子邮件中解释的问题。 “黑客攻击邮件服务器。”',
    'home' => '家',
    'newsandevent'=>'新闻事件',
    'drinks' => '飲料',
    'video' => '影片',
    'standard' => '標準',
    'contact' => '聯繫',
    '1688' => '1688 公司環球有限公司',
    '13/47' => '47分之13哞7 Navamin酒店Klongkum Bueng琴區，曼谷10230',
    'phone' => '電話',
    'fax' => '傳真',
    'worker' => '工人',
    'sports' => '體育',
    'agriculture' => '農業',
    'education' => '研究',
    'videos' => '視頻',
    'map' => '地圖',
    'fillContact' => '請在下面的字段填寫完成的便利。我們將盡快結束送還給你。',
    
    'benefit' => '好處',
    'convenient' => '方便',
    'share' => '分享',
    'by' => '通過',

    'sucrose' => '蔗糖',
    'b3' => '維生素 B3',
    'b5' => '維生素 B5',
    'b6' => '維生素 B6',
    'dexpanthenol' => '泛醇',
    'b12' => '維生素B12',
    'taurine' => '牛磺酸',
    'caffeine' => '咖啡因',
    'Inositol' => '肌醇',
    'vitaminC' => '維生素C',
    'fiber' => '纖維',
    'ingredient' => '成分',
    'sugar' => '糖',
    'artificial' => '自然色人工香精防腐劑'

    ];