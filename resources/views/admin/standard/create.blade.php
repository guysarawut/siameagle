@extends('layouts.admin.theme')

@section('page_title', 'Convenient For')

@section('content')
  <div class="page-header">
    <h3>
      สร้างข้อความ มาตรฐานการผลิต
    </h3>
  </div>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <div class="row">
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
      @include('admin.standard._form')
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default">Add Standard</button>
        </div>
      </div>
    </form>
  </div>
@stop
