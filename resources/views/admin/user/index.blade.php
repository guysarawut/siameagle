@extends('layouts.admin.theme')

@section('page_title', 'Member')

@section('content')
  <div class="page-header">
    <h3>
      จัดการสมาชิก
      <a href="{{ url('admin/user/create') }}"><small class="pull-right btn btn-default"><i class="fa fa-users"></i> Create Member</small></a>
    </h3>
  </div>

  <div cass="row">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="{{ Request::segment(4) == 1 ? 'active' : '' }}"><a href="{{ url('admin/user/show/1') }}">Super Admin</a></li>
      <li role="presentation" class="{{ Request::segment(4) == 2 ? 'active' : '' }}"><a href="{{ url('admin/user/show/2') }}">Admin</a></li>
      
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active">
        @if( count($members) != 0 )
          <div class="table-responsive" style="margin-top: 10px;">
            <table class="table table-striped">
              <thead>
                <th>No #</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($members as $index => $member)
                  <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $member->firstname }}</td>
                    <td>{{ $member->lastname }}</td>
                    <td>{{ $member->email }}</td>
                    <td>
                      <a href="{{ url("admin/user/edit/$member->id") }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o"></i> Edit</a>
                      <a href="{{ url("admin/user/delete/$member->id") }}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @else
          <h3>No Data</h3>
        @endif
      </div>
    </div>
  </div>
@stop

@section('custom-js')
  <script type="text/javascript">
    $(function() {
      $('a.delete').click(function() {
        if( confirm("Delete User") ) {
          return true;
        }
        else {
          return false;
        }
      })
    });
  </script>
@stop
