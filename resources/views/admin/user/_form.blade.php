<div class="form-group {{ $errors->first('email') != '' ? 'has-error' : '' }}">
  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
  <div class="col-sm-5">
    <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" value="{{ !empty($member) ? $member->email : old('email') }}">
  </div>
</div>
<div class="form-group {{ $errors->first('password') != '' || !empty(session('message')) ? 'has-error' : '' }}">
  <label for="inputPassword" class="col-sm-2 control-label">Password</label>
  <div class="col-sm-5">
    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
    @if( !empty($member) )
      <span class="text-success">If you want to change your password, enter your password.</span>
    @endif
  </div>
</div>
<div class="form-group {{ $errors->first('re_password') != '' || !empty(session('message')) ? 'has-error' : '' }}">
  <label for="inputPassword" class="col-sm-2 control-label">Re Password</label>
  <div class="col-sm-5">
    <input type="password" class="form-control" id="inputPassword" name="re_password" placeholder="Re Password">
  </div>
</div>
<div class="form-group {{ $errors->first('firstname') != '' ? 'has-error' : '' }}">
  <label for="inputFirstname" class="col-sm-2 control-label">Firstname</label>
  <div class="col-sm-5">
    <input type="text" class="form-control" id="inputFirstname" name="firstname" placeholder="Firstname" value="{{ !empty($member) ? $member->firstname : old('firstname') }}">
  </div>
</div>
<div class="form-group {{ $errors->first('lastname') != '' ? 'has-error' : '' }}">
  <label for="inputLastname" class="col-sm-2 control-label">Lastname</label>
  <div class="col-sm-5">
    <input type="text" class="form-control" id="inputLastname" name="lastname" placeholder="Lastname" value="{{ !empty($member) ? $member->lastname : old('lastname') }}">
  </div>
</div>
<div class="form-group">
  <label for="select" class="col-sm-2 control-label">News & Event</label>
  <div class="col-sm-5">
    
    <select class="form-control" id="select" name="role">
      <option value="1">Read & Write</option>
      <option value="3">Read Only</option>
    </select>
   
    
   
    <br>
  </div>
</div>
<div class="form-group">
  <label for="select" class="col-sm-2 control-label">Video</label>
  <div class="col-sm-5">
   
    <select class="form-control" id="select" name="role">
      <option value="1">Read & Write</option>
      <option value="3">Read Only</option>
    </select>
   
    <br>
  </div>
</div>
<div class="form-group">
  <label for="select" class="col-sm-2 control-label">Type Member</label>
  <div class="col-sm-5">
    @if( !empty($member) )
    <select class="form-control" id="select" name="role">
      <option value="1" {{ $member->role == '1' ? 'selected' : '' }}>Super Admin</option>
      <option value="2" {{ $member->role == '2' ? 'selected' : '' }}>Admin</option>
      
    </select>
    @else
    <select class="form-control" id="select" name="role">
      <option value="1" {{ old('user_role') == '1' ? 'selected' : '' }}>Super Admin</option>
      <option value="2" {{ old('user_role') == '2' ? 'selected' : '' }}>Admin</option>
      
    </select>
    @endif
    <br>
  </div>
</div>
