@extends('layouts.admin.theme')
@section('page_title','Video')
@section('custom-css')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
@stop
@section('content')
<div class="page-header">
  <h3>
    หน้าจัดการวิดีโอ
    <a href="{{ url('admin/video/create') }}"><small class="pull-right btn btn-default"><i class="fa fa-plus-square"></i> Add Video</small></a>
  </h3>
</div> 
<div class="row">
    <div class="col-md-12">
      @foreach($videos as $index => $video)
      <div class="col-xs-12 col-md-12" style="margin-top:20px;">
        <div class="">
          <p style="text-align:center;"><span class="label label-default">TH</span> {{ $video->video_name_th }}</p>
          <p style="text-align:center;"><span class="label label-default">EN</span> {{ $video->video_name_en }}</p>
          <p style="text-align:center;"><span class="label label-default">CN</span> {{ $video->video_name_cn }}</p>
          
          <div class="col-md-4 thumbnail">
          
          <img src="http://img.youtube.com/vi/{{ $video->video_idTH }}/0.jpg" alt="...">
          <span class="label label-default">TH</span>         
          </div>
          
          <div class="col-md-4 thumbnail">
          
          <img src="http://img.youtube.com/vi/{{ $video->video_idEN }}/0.jpg" alt="..."> 
          <span class="label label-default">EN</span>                     
          </div>
          
          <div class="col-md-4 thumbnail">
          
          <img src="http://img.youtube.com/vi/{{ $video->video_idCN }}/0.jpg" alt="...">
          <span class="label label-default">CN</span>
          </div>
          
          
          
        </div>

        <div class="col-md-12" style="text-align:center;">
          <!-- <button type="button" class="btn btn-primary">อัพเดต</button> -->
          <a href="{{ url("admin/video/edit/$video->id") }}" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> อัพเดต</a>

          <!-- <button type="button" class="btn btn-danger">ลบวิดีโอนี้</button> -->
          <a href="{{ url("admin/video/delete/$video->id") }}" class="btn btn-danger delete"><i class="fa fa-trash-o"></i> ลบ</a>
           <hr>
          <!-- <div style="padding: 5px;">
          <input class=""type="checkbox" name="" value="">ตั้งเป็น Video Highlight
          </div> -->
        </div>


      </div>

      @endforeach
     <!-- <?php for ($i=0; $i <= 11 ; $i++) { ?> -->
          <!-- <div class="col-xs-6 col-md-3" style="margin-top:20px;">
            <a href="https://www.youtube.com/watch?v=3FeiyzZffJ8" class="thumbnail">
              <p style="text-align:center;">TOP 10 Most Powerful SITH LORDS</p>
              <img src="http://img.youtube.com/vi/3FeiyzZffJ8/0.jpg" alt="...">
            </a>

            <div class="" style="text-align:center;">
              <button type="button" class="btn btn-primary">อัพเดต</button>
              <button type="button" class="btn btn-danger">ลบวิดีโอนี้</button>
              <div>
              <input class=""type="checkbox" name="" value="">ตั้งเป็น Video Highlight
              </div>
            </div>

          </div> -->
      <!-- <?php } ?> -->
    </div>
</div>

<!-- <script>
$('input:checkbox').on('change', function(evt) {
 if($(this).siblings(':checked').length >= 2) {
     this.checked = false;
     console.log(this.value)
 }
});
</script> -->

<script>
$('input:checkbox').on('change', function(evt) {
 if($('input:checkbox:checked').length >= 3) {
     this.checked = false;
     console.log(this.value)
 }
});
</script>


@stop

@section('custom-js')
  <script type="text/javascript">
    $(function() {
      $('a.delete').click(function() {
        if( confirm("คุณต้องการจะลบวิดีโอนี้") ) {
          return true;
        }
        else {
          return false;
        }
      })
    });
  </script>
@stop
