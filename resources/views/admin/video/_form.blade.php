<div class="form-group">
  <label for="" class="col-sm-2 control-label">ชื่อวิดีโอ (TH)</label>
  <div class="col-sm-8">
    <input type="text" name="video_name_th" class="form-control" id="" placeholder="" 
    value="{{ !empty($videos) ? $videos->video_name_th : old('video_name_th') }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">ชื่อวิดีโอ (EN)</label>
  <div class="col-sm-8">
    <input type="text" name="video_name_en" class="form-control" id="" placeholder=""
    value="{{ !empty($videos) ? $videos->video_name_en : old('video_name_en') }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">ชื่อวิดีโอ (CN)</label>
  <div class="col-sm-8">
    <input type="text" name="video_name_cn" class="form-control" id="" placeholder=""
    value="{{ !empty($videos) ? $videos->video_name_cn : old('video_name_cn') }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">URL วิดีโอ youtube (TH)</label>
  <div class="col-sm-8">
    <input type="text" name="url_th" class="form-control" id="" placeholder=""
    value="{{ !empty($videos) ? $videos->url_th : old('url_th') }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">URL วิดีโอ youtube (EN)</label>
  <div class="col-sm-8">
    <input type="text" name="url_en" class="form-control" id="" placeholder=""
    value="{{ !empty($videos) ? $videos->url_en : old('url_en') }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">URL วิดีโอ youtube (CN)</label>
  <div class="col-sm-8">
    <input type="text" name="url_cn" class="form-control" id="" placeholder=""
    value="{{ !empty($videos) ? $videos->url_cn : old('url_cn') }}">
  </div>
</div>
