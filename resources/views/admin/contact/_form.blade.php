<div class="form-group">
  <label for="" class="col-sm-2 control-label">ชื่อบริษัท (TH)</label>
  <div class="col-sm-8">
    <input type="text" name="company_name_th" class="form-control" id="" placeholder="" value="{{ !empty($contact) ? $contact->company_name_th : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">ที่อยู่บริษัท (TH)</label>
  <div class="col-sm-8">
    <input type="text" name="company_address_th" class="form-control" id="" placeholder=""value="{{ !empty($contact) ? $contact->company_address_th : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">ชื่อบริษัท (EN)</label>
  <div class="col-sm-8">
    <input type="text" name="company_name_en" class="form-control" id="" placeholder=""value="{{ !empty($contact) ? $contact->company_name_en : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">ที่อยู่บริษัท (EN)</label>
  <div class="col-sm-8">
    <input type="text" name="company_address_en" class="form-control" id="" placeholder=""value="{{ !empty($contact) ? $contact->company_address_en : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">เบอร์โทรศัพท์</label>
  <div class="col-sm-8">
    <input type="text" name="company_tel" class="form-control" id="" placeholder="" value="{{ !empty($contact) ? $contact->company_tel : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">เบอร์แฟกซ์</label>
  <div class="col-sm-8">
    <input type="text" name="company_fax" class="form-control" id="" placeholder="" value="{{ !empty($contact) ? $contact->company_fax : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">E-Mail</label>
  <div class="col-sm-8">
    <input type="text" name="company_email" class="form-control" id="" placeholder="" value="{{ !empty($contact) ? $contact->company_email : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">เวลาทำการ</label>
  <div class="col-sm-8">
    <input type="text" name="company_fooice_house" class="form-control" id="" placeholder="" value="{{ !empty($contact) ? $contact->company_fooice_house : '' }}">
  </div>
</div>
