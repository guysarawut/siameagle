@extends('layouts.admin.theme')

@section('page_title', 'Contact')

@section('content')
  <div class="page-header">
    <h3><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbspจัดการหน้า ติดต่อเรา</h3>
  </div>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <div class="row">
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
      @include('admin.contact._form')
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-primary">Add Contact</button>
        </div>
      </div>
    </form>
  </div>
@stop
