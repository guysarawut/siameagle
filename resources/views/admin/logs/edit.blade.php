@extends('layouts.admin.theme')
@section('page_title', 'Edit Member')
@section('content')
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  @if (session('message'))
    <div class="alert alert-danger">
      <ul>
        <li>{{ session('message') }}</li>
      </ul>
    </div>
  @endif

  <div class="well bs-component">
    <form class="form-horizontal" method="post">
      <fieldset>
        <legend><i class="fa fa-pencil-square-o"></i> Edit Member</legend>
        @include('admin.user._form')
        <div class="form-group">
          <div class="col-sm-10 col-sm-offset-2">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit Member</button>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
@stop
