@extends('layouts.admin.theme')

@section('page_title', 'Logs')
@section('content')
  <div class="page-header">
    <h3>
      Logs
    </h3>
  </div>

  <table class="table">
  <thead class="thead-inverse">
    <tr>
      <th>No.</th>
      <th>User ID</th>
      <th>Log</th>
      <th>IP</th>
      <th>Date Time</th>
    </tr>
  </thead>
  <tbody>
  @foreach($logs as $index => $log)
    <tr>
      <th scope="row">{{$log->id}}</th>
      <td>{{$log->user_id}}</td>
      <td>{{$log->text}}</td>
      <td>{{$log->ip_address}}</td>
      <td>{{$log->created_at}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
{!! $logs->render() !!}
@stop

@section('custom-js')
  <script type="text/javascript">
    $(function() {
      $('a.delete').click(function() {
        if( confirm("Delete User") ) {
          return true;
        }
        else {
          return false;
        }
      })
    });
  </script>
@stop
