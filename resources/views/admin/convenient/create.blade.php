@extends('layouts.admin.theme')

@section('page_title', 'Convenient For')

@section('content')
  <div class="page-header">
    <h3>
      สร้างข่าวและกิจกรรม
    </h3> 
  </div>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <div class="row">
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
      @include('admin.convenient._form')
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default">Add</button>
        </div>
      </div>
    </form>
  </div>
@stop
