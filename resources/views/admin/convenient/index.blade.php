@extends('layouts.admin.theme')

@section('page_title', 'News & Event')

@section('content')
  <div class="page-header">
    <h3>
      จัดการหน้า ข่าวและกิจกรรม
      <a href="{{ url('admin/convenient/create') }}"><small class="pull-right btn btn-default"><i class="fa fa-plus-square"></i> Create</small></a>
    </h3>
  </div> 
  <div class="row">
    <div class="col-md-12">
      
      <div class="panel panel-warning convenient">
        
        <div class="panel-body">
          
<div class="row">
    <div class="col-md-12">
      @foreach($convenients as $index => $convenient)
      <div class="col-xs-12 col-md-12" style="margin-top:20px;">
        <div class="">
        
          <div class="col-md-4 thumbnail">
          
          <img style="width: 480px;height: 360px;" src="{{ url("images/upload/$convenient->convenient_image_th") }} " alt="...">
          
          <blockquote>
            <p style=""><span class="label label-default">หัวข้อ TH</span> {{ $convenient->convenient_name_th }}</p>
          
          </blockquote>
          <p>
            <strong>รายละเอียด (TH)</strong><br>
            <span>{{ $convenient->convenient_detail_th }}</span>
          </p>        
          </div>
          
          <div class="col-md-4 thumbnail">
          
          <img style="width: 480px;height: 360px;" src="{{ url("images/upload/$convenient->convenient_image_en") }}" alt="..."> 
          <blockquote>
          <p style=""><span class="label label-default">หัวข้อ EN</span> {{ $convenient->convenient_name_en }}</p>
          </blockquote>
          <p>
            <strong>รายละเอียด (EN)</strong><br>
            <span>{{ $convenient->convenient_detail_en }}</span>
          </p>                   
          </div>
          
          <div class="col-md-4 thumbnail">
          
          <img style="width: 480px;height: 360px;" src="{{ url("images/upload/$convenient->convenient_image_cn") }} " alt="...">
          <blockquote>
          <p style=""><span class="label label-default">หัวข้อ CN</span> {{ $convenient->convenient_name_cn }}</p>
          </blockquote>
          <p>
            <strong>รายละเอียด (CN)</strong><br>
            <span>{{ $convenient->convenient_detail_cn }}</span>
          </p>
          </div>
          
          
          
        </div>

        <div class="col-md-12" style="text-align:center;">
          <!-- <button type="button" class="btn btn-primary">อัพเดต</button> -->
          <a href="{{ url("admin/convenient/update/$convenient->id") }}" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> อัพเดต</a>

          <!-- <button type="button" class="btn btn-danger">ลบวิดีโอนี้</button> -->
          <a href="{{ url("admin/convenient/delete/$convenient->id") }}" class="btn btn-danger delete"><i class="fa fa-trash-o"></i> ลบ</a>
           <hr>
          <!-- <div style="padding: 5px;">
          <input class=""type="checkbox" name="" value="">ตั้งเป็น convenient Highlight
          </div> -->
        </div>


      </div>

      @endforeach
     <!-- <?php for ($i=0; $i <= 11 ; $i++) { ?> -->
          <!-- <div class="col-xs-6 col-md-3" style="margin-top:20px;">
            <a href="https://www.youtube.com/watch?v=3FeiyzZffJ8" class="thumbnail">
              <p style="text-align:center;">TOP 10 Most Powerful SITH LORDS</p>
              <img src="3FeiyzZffJ8/0.jpg" alt="...">
            </a>

            <div class="" style="text-align:center;">
              <button type="button" class="btn btn-primary">อัพเดต</button>
              <button type="button" class="btn btn-danger">ลบวิดีโอนี้</button>
              <div>
              <input class=""type="checkbox" name="" value="">ตั้งเป็น convenient Highlight
              </div>
            </div>

          </div> -->
      <!-- <?php } ?> -->
    </div>
</div>
          
        </div>
      </div>
      


    </div>
  </div>
@stop

@section('custom-js')
  <script type="text/javascript">
    $(function() {
      $('a.delete').click(function() {
        if( confirm("คุณต้องการจะลบข่าวนี้") ) {
          return true;
        }
        else {
          return false;
        }
      })
    });
  </script>
@stop
