<div class="form-group">
  <label for="" class="col-sm-2 control-label">หัวข้อข่าวสาร (TH)</label>
  <div class="col-sm-8">
    <input type="text" name="convenient_name_th" class="form-control" id="" placeholder="" value="{{ !empty($convenient) ? $convenient->convenient_name_th : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">รายละเอียด (TH)</label>
  <div class="col-sm-8">
    <textarea name="convenient_detail_th" rows="6" class="form-control">{{ !empty($convenient) ? $convenient->convenient_detail_th : '' }}</textarea>
  </div> 
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">หัวข้อข่าวสาร (EN)</label>
  <div class="col-sm-8">
    <input type="text" name="convenient_name_en" class="form-control" id="" placeholder="" value="{{ !empty($convenient) ? $convenient->convenient_name_en : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">รายละเอียด (EN)</label>
  <div class="col-sm-8">
    <textarea name="convenient_detail_en" rows="6" class="form-control">{{ !empty($convenient) ? $convenient->convenient_detail_en : '' }}</textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">หัวข้อข่าวสาร (CN)</label>
  <div class="col-sm-8">
    <input type="text" name="convenient_name_cn" class="form-control" id="" placeholder="" value="{{ !empty($convenient) ? $convenient->convenient_name_cn : '' }}">
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-2 control-label">รายละเอียด (CN)</label>
  <div class="col-sm-8">
    <textarea name="convenient_detail_cn" rows="6" class="form-control">{{ !empty($convenient) ? $convenient->convenient_detail_cn : '' }}</textarea>
  </div>
</div>
<!-- <div class="form-group">
<label for="" class="col-sm-2 control-label">หมวดหมู่</label>
<div class="col-md-8">
<select class="form-control">
  <option>คนทำงาน</option>
  <option>กีฬา</option>
  <option>เกษตรกรรม</option>
  <option>การศึกษา</option>
</select>
</div>
</div> -->
<div class="form-group">
  <label for="" class="col-sm-2 control-label">รูปภาพ (TH)</label>
  <div class="col-sm-6">
    @if( !empty($convenient) )
      @if( $convenient->convenient_image_th != "" )
        <img class="img-responsive" src="{{ url("images/upload/$convenient->convenient_image_th") }}" alt="" />
        <div class="">
          <strong style="margin: 10px 0 10px 0; display: block;">หากคุณต้องการเปลี่ยนรูปภาพกรุณาเลือกภาพใหม่</strong>
        </div>
      @endif
    @endif
    <input type="file" name="image" class="form-control" id="" placeholder="">
  </div>
</div>

<div class="form-group">
  <label for="" class="col-sm-2 control-label">รูปภาพ (EN)</label>
  <div class="col-sm-6">
    @if( !empty($convenient) )
      @if( $convenient->convenient_image_en != "" )
        <img class="img-responsive" src="{{ url("images/upload/$convenient->convenient_image_en") }}" alt="" />
        <div class="">
          <strong style="margin: 10px 0 10px 0; display: block;">หากคุณต้องการเปลี่ยนรูปภาพกรุณาเลือกภาพใหม่</strong>
        </div>
      @endif
    @endif
    <input type="file" name="image_en" class="form-control" id="" placeholder="">
  </div>
</div>

<div class="form-group">
  <label for="" class="col-sm-2 control-label">รูปภาพ (TH)</label>
  <div class="col-sm-6">
    @if( !empty($convenient) )
      @if( $convenient->convenient_image_cn != "" )
        <img class="img-responsive" src="{{ url("images/upload/$convenient->convenient_image_cn") }}" alt="" />
        <div class="">
          <strong style="margin: 10px 0 10px 0; display: block;">หากคุณต้องการเปลี่ยนรูปภาพกรุณาเลือกภาพใหม่</strong>
        </div>
      @endif
    @endif
    <input type="file" name="image_cn" class="form-control" id="" placeholder="">
  </div>
</div>

<div class="form-group">
  <label for="select" class="col-sm-2 control-label">หมวดหมู่</label>
  <div class="col-sm-5">
    @if( !empty($convenient) )
    <select class="form-control" id="select" name="category">
      <option value="1" {{ $convenient->category == '1' ? 'selected' : '' }}>คนทำงาน</option>
      <option value="2" {{ $convenient->category == '2' ? 'selected' : '' }}>กีฬา</option>
      <option value="3" {{ $convenient->category == '3' ? 'selected' : '' }}>เกษตรกรรม</option>
      <option value="4" {{ $convenient->category == '4' ? 'selected' : '' }}>การศึกษา</option>
    </select>
    @else
    <select class="form-control" id="select" name="category">
      <option value="1" {{ old('category_role') == '1' ? 'selected' : '' }}>คนทำงาน</option>
      <option value="2" {{ old('category_role') == '2' ? 'selected' : '' }}>กีฬา</option>
      <option value="3" {{ old('category_role') == '3' ? 'selected' : '' }}>เกษตรกรรม</option>
      <option value="4" {{ old('category_role') == '4' ? 'selected' : '' }}>การศึกษา</option>
      
    </select>
    @endif
    <br>
  </div>
</div>

