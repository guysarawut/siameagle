@extends('layouts.main')
@section('title','Home')
@section('style')
<style>
/* News */
.news-slider {
/* Style the actual content */
}
.news-slider .text-content {
position: absolute;
top: 0;
left: 0;

right: 0;
/*background-color: rgba(255, 255, 255, 0.9);*/
background-color: #fff;
padding: 8em 0.8em;
width: 40%;
height: 100%;
-webkit-clip-path: polygon(0 0, 43% 0, 100% 100%, 0% 100%);
clip-path: polygon(0 0, 43% 0, 100% 100%, 0% 100%);
}
.news-slider .text-content h2.home--h2 {
/*margin: 0 0 0 0;*/
padding:0 0 0 1em;
background: rgba(233,28,40,0.7);
width: 60%;


}
.news-slider .text-content h2.home--h2:hover {
/*margin: 0 0 0 0;*/
/*padding:0 0 1em 1em;*/
background: #f5cb25;
}
.news-slider .text-content h2.home--h2 a:hover {
/*margin: 0 0 0 0;*/
/*padding:0 0 1em 1em;*/
color:rgb(233,28,40);
text-decoration: none;
}

.news-slider .text-content p {
margin: 1em 0;
line-height: 1.5;
}
.news-slider .text-content a.button-link {
padding: 0.25em 0.5em;
position: absolute;
bottom: 1em;
right: 1em;
}
.news-slider .image-content {
line-height: 0;
}
.news-slider .image-content img {
width: 100%;
height: 30em;
}
.news-slider .news-pager {
text-align: left;
display: block;
margin: 0.2em 3em 0;
padding: 0;
list-style: none;
}
.news-slider .news-pager li {
display: inline-block;
padding: 0.4em;
/*margin: 0 0 0 1em;*/
}
.news-slider .news-pager li.sy-active a {
color: #e91c28;
}
.news-slider .news-pager li a {
font-weight: 500;
font-size: 2em;
text-decoration: none;
display: block;
color: gray;
margin: -6px;
}
</style>
 <script type="text/javascript">
 $(document).ready(function(){

   jQuery('#news-demo').slippry({
  // general elements & wrapper
  slippryWrapper: '<div class="sy-box news-slider" />', // wrapper to wrap everything, including pager
  elements: 'article', // elments cointaining slide content

  // options
  adaptiveHeight: false, // height of the sliders adapts to current
  captions: false,

  // pager
  pagerClass: 'news-pager',

  // transitions
  transition: 'fade', // fade, horizontal, kenburns, false
  speed: 1200,
  pause: 8000,

  // slideshow
  autoDirection: 'next'
  });
});

 </script>
 @stop
 @section('content')

<div class="sections">
    <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div style="padding-bottom:30%;"> </div>

                    <div class="row" style="top:200px;">
                    <div class="col-md-6"><span><center><img class="img-responsive " src="{{asset('img/star_left.png')}}" alt="" style="position:relative;top:-20em;left:5em;" /></center> </span></div>
                    <div class="col-md-6"><span><center><img class="img-responsive " src="{{asset('img/star_right.png')}}" alt="" style="position:relative;top:-20em;right:5em;" /></center> </span></div>
                    </div>
                    <div id="" class="animated "><img src="" alt="" />
                    </div>
                    <div class="non_animated bottle"><img src="" alt="" />
                    </div>

                </div>
            </div>
        </div>





        <span><img class="img-responsive can01_title--480" src="{{asset('img/c1_img_can01_title_480.png')}}" alt=""/> </span>
        <div class="container">
            <div class="row" style="bottom:340px;">
                <div class="col-xs-12 col-sm-6 div_right">
<!--                    <strong>  เติมความสดชื่น ตื่นจากความอ่อนล้า <br>เพิ่มการเผาผลาญไขมัน และฟื้นฟูในระดับเซลล์ <br>ใส่คำบรรยายสรรพคุณสินค้านี้ต่อไปเรื่อยๆ</strong>-->
                </div>
                <div class="col-xs-12 col-sm-6 div_left">
<!--                    <strong>  บำรุงสายตา ช่วยให้สดชื่น ตื่นตัวอยู่เสมอ <br>บำรุงระบบประสาทและสมอง และระบบขับถ่าย<br>ดื่มกี่ขวดก็ได้ไม่จำกัด</strong>-->
                </div>
            </div>

        </div>

        <div class="container">
            <div class="row" style="bottom:200px;">
                <div class="col-xs-12 div_480">
<!--
                    <strong>  เติมความสดชื่น ตื่นจากความอ่อนล้า <br>เพิ่มการเผาผลาญไขมัน และฟื้นฟูในระดับเซลล์ <br>ใส่คำบรรยายสรรพคุณสินค้านี้ต่อไปเรื่อยๆ<br>
                     บำรุงสายตา ช่วยให้สดชื่น ตื่นตัวอยู่เสมอ <br>บำรุงระบบประสาทและสมอง และระบบขับถ่าย<br>ดื่มกี่ขวดก็ได้ไม่จำกัด</strong>
-->
                </div>
            </div>

        </div>

    </section>

    <section class="row" id="sec01">
        <div class="container">
            <div class="row">
                <h1><img src="{{asset('img/c2_ico_news.png')}}" alt="" />&nbsp{{ trans('messages.newsandevent') }}</h1>

                <section id="news-demo">
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล...</p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="{{asset('img/album_other-20110506-175010')}}-3.jpg" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล...  </p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="{{asset('img/article-71342_1280.jpg')}}" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล... </p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="{{asset('img/photographer-410326_1280.jpg')}}" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล...</p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="{{asset('img/album_other-20110506-175010')}}-3.jpg" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล... </p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="{{asset('img/photographer-410326_1280.jpg')}}" alt=""></div>
                  </article>
                </section>

                <!-- <div id="slides" style="margin-bottom:50px;">
                <a href="news.php">  <img src="img/example-slide-1.jpg" alt=""></a>
                    <img src="img/example-slide-2.jpg" alt="">
                    <img src="img/example-slide-3.jpg" alt="">
                    <img src="img/example-slide-4.jpg" alt="">
                </div> -->

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <h1>{{ trans('messages.worker')}}</h1>
                </div>
            </div>
            <div class="row">

              @foreach($news  as $data)
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="{{asset('images/upload/'.$data->convenient_image)}}" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">{{$data->convenient_name}}</a></h3>
                                    <p>{{$data->convenient_detail}}</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach


            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;top:0;">ดูเพิ่มเติม</h4></a>
            <div class="row">
                <div class="col-sm-6">
                    <h1>{{ trans('messages.sports')}}</h1>
                </div>
            </div>
            <div class="row">
              @foreach($news  as $data)
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="{{asset('images/upload/'.$data->convenient_image)}}" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">{{$data->convenient_name}}</a></h3>
                                    <p>{{$data->convenient_detail}}</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            <div class="row">
                <div class="col-sm-6">
                    <h1>{{ trans('messages.agriculture')}}</h1>
                </div>
            </div>
            <div class="row">
                @foreach($news  as $data)
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="{{asset('images/upload/'.$data->convenient_image)}}" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">{{$data->convenient_name}}</a></h3>
                                    <p>{{$data->convenient_detail}}</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach

                
            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            <div class="row">
                <div class="col-sm-6">
                    <h1>{{ trans('messages.education')}}</h1>
                </div>
            </div>
            <div class="row">
                @foreach($news  as $data)
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="{{asset('images/upload/'.$data->convenient_image)}}" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">{{$data->convenient_name}}</a></h3>
                                    <p>{{$data->convenient_detail}}</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach

            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            </div>

    </section>
    <!-- END row -->

    <section class="row" id="sec02">
        <div class="container">
            <div class="row">
                <h1><img src="{{ asset('img/c2_ico_vdo.png')}}" alt="" />&nbsp{{ trans('messages.videos')}}</h1>
                <div class="row">
                
                @foreach($videos  as $videodata)
                 
                      <div class="col-xs-12 col-md-6">
                        <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                          <div style="cursor:pointer;" class="vid-item">
                            <div class="thumb">
                              <a href="" class=""><img src="http://img.youtube.com/vi/{{ $videodata -> video_id }}/0.jpg"></a>
                            </div>
                            <!-- <div class="desc">
                              Anakin Skywalker vs. Obi-Wan Kenobi
                            </div> -->
                            
                            <div class="col-sm-6" style="float:none;margin: 0 auto;">
                                <p style="color:red;text-align:center;">{{ $videodata -> video_name}}<br>
                                  <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครั้ง - 3 สัปดาห์ที่ผ่านมา...</span>
                                </p>

                            </div>
                           
                          </div>
                        </div>

                      </div>
                     @endforeach

                      <!-- <div class="col-xs-12 col-md-6">
                        <a href="#" class="thumbnail">
                          <img src="img/iphone-6s-11.jpg" alt="...">
                        </a>
                        <div class="col-sm-6" style="float:none;margin: 0 auto;">
                            <p style="color:red;text-align:center;">โครงการปลูกต้นไม้เฉลิมพระเกียรติ...<br>
                              <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครั้ง - 3 สัปดาห์ที่ผ่านมา...</span>
                            </p>

                        </div>

                      </div> -->
                      
                  </div>

                <a href="content_list.php"><h4 style="color:#4c4c4c;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            </div>
        </div>
    </section>
    <!-- END row -->

    <section class="row" id="envelope">
        <div class="container">
            <div class="row">





            <div class="container">
                <div class="row benefit_top--img">
                </div>
            </div>

                    <div class="row benefit_top benefit__ul--stroke" style="text-align:center;color:#fff;">
                        <div class="col-sm-6">
                            <p class="benefit_h1 stroke" style="font-size: 3em;"><strong style="margin-right: 70%;">{{ trans('messages.benefit')}}</strong></p>
                            <div class="row benefit_ul" style="text-align:left;">
                                <div class="col-sm-6">
                                    <ul class="color555" style="list-style:none;font-size:20px;">
                                        <li><a href="#111">{{ trans('messages.sucrose')}}</a>
                                        </li>
                                        <li><a href="#222">{{ trans('messages.b3')}}</a>
                                        </li>
                                        <li><a href="#333">{{ trans('messages.dexpanthenol')}}</a>
                                        </li>
                                        <li><a href="#444">{{ trans('messages.b12')}}</a>
                                        </li>
                                        <li><a href="#555">{{ trans('messages.taurine')}}</a>
                                        </li>
                                        <li><a href="#666">{{ trans('messages.caffeine')}}</a>
                                        </li>
                                        <li><a href="#777">{{ trans('messages.Inositol')}}</a>
                                        </li>
                                    </ul>
                                </div>

                            </div>



                        </div>
                        <div class="col-sm-6">
                            <p class="benefit_h1 stroke" style="font-size: 3em;"><strong style="margin-left: 65%;">{{ trans('messages.convenient')}}</strong></p>
                            <div class="col-sm-6 ">
                                <div style="line-height: 1;font-size:1.3em;max-width:50%;color:#4c4c4c;text-align:left;">เครื่องดื่มอินทรีสยามเหมาะกับทุกเพศ ทุกวัย และทุกสาขาอาชีพ</div>


                            </div>


                        </div>

                    </div>


                        <div class="row benefit_margin-top" style="text-align:center;">
                          <div class="col-md-12">
                             <center> <p class="" style="font-size:3em;color:#555;">{{ trans('messages.contact')}}</p> </center>
                          </div>
                            <div class="col-sm-12">
                              <img style="width:280px;height:180px;" src="{{asset('img/LOGo.png')}}" alt="" />
                            </div>

                            <div class="col-sm-12">
                            <div>
                                <h1 style="color:gray;">02-510-3000</h1>
                            </div>
                            </div>

                       </div>




                </div>
            </div>
        </div>
@stop
