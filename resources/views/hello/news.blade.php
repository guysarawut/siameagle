@extends('layouts.main')
@section('title','News')
@section('style')
<style>
  hr {
    color: #fff;
    border:3px solid;
  }
  /* Circular Content Carousel Style */
  .ca-container{
  	position:relative;
  	margin:25px auto 20px auto;
  	width:990px;
  	height:450px;
  }
  .ca-wrapper{
  	width:100%;
  	height:100%; 
  	position:relative;
  }
  .ca-item{
  	position:relative;
  	float:left;
  	width:330px;
  	height:100%;
  	text-align:center;
  }

  .ca-item-main{
  	padding:20px;
  	position:absolute;
  	top:5px;
  	left:5px;
  	right:5px;
  	bottom:5px;
  	overflow:hidden;

  }
  .ca-icon{
  	width:100%;
  	height:189px;
  	position:relative;
  	margin:0 auto;
  	background:transparent url(img/iphone-6s-11.jpg) no-repeat center center;
  }
  .ca-icon img{
  	width:100%;
    height:189px;
  }
  
  .ca-item h4{
  	font-size:12px;
  	text-align:left;
/*  	padding-left:10px;*/
  	line-height:16px;
/*  	margin:10px;*/
  	position:relative;
  }
  .ca-item h4 span{
  	display:block;
      color: rgba(0,0,0,0.3);
  }
  .ca-content{
  	width:660px;
  	overflow:hidden;
  }
  .ca-content-text{
  	font-size: 14px;
  	font-style: italic;
  	font-family: "Georgia","Times New Roman",serif;
  	margin:10px 20px;
  	padding:10px 20px;
  	line-height:24px;
  }
  .ca-content-text p{
  	padding-bottom:5px;
  }
  .ca-content h6{
  	margin:25px 20px 0px 35px;
  	font-size:32px;
  	padding-bottom:5px;
  	color:#000;
  	font-family: 'Coustard', sans-serif;
  	color:#60817a;
  	border-bottom:2px solid #99bcb4;
  	text-shadow: 1px 1px 1px #99BCB4;
  }
  .ca-content ul{
  	margin:20px 35px;
  	height:30px;
  }
  .ca-content ul li{
  	float:left;
  	margin:0px 2px;
  }
  .ca-content ul li a{
  	color:#fff;
  	background:#000;
  	padding:3px 6px;
  	font-size:14px;
  	font-family: "Georgia","Times New Roman",serif;
  	font-style:italic;
  }
  .ca-content ul li a:hover{
  	background:#fff;
  	color:#000;
  	text-shadow:none;
  }
  .ca-nav span{
  	width:25px;
  	height:38px;
  	background:transparent url(img/arrows.png) no-repeat top left;
  	position:absolute;
  	top:30%;
  	margin-top:-19px;
  	left:-40px;
  	text-indent:-9000px;
  	opacity:0.7;
  	cursor:pointer;
  	z-index:100;
  }
  .ca-nav span.ca-nav-next{
  	background-position:top right;
  	left:auto;
  	right:-40px;
  }
  .ca-nav span:hover{
  	opacity:1.0;
  }
  </style>
@stop
@section('content')
<div class="sections">
    <section id="hero-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="row" style="color:#4c4c4c;text-align:left;">
                        <img class="img-responsive" src="{{asset('img/iphone-6s-11.jpg')}}" alt="" style="margin:0 auto;height:65%;margin-top: 5%;" />
                        <div class="col-sm-12">
                          <p class="stroke Prapakorn-0" style="line-height:0.8;font-size:36px;margin:30px 0;">ดวลแข้งฟุตซอลรอบชิงฯ "อินทรีสยาม แชมเปี้ยน 2015" ชิงถ้วยรางวัลและทุนการศึกษามูลค่ากว่า 50,000 บาท </p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="padding-bottom:0;">
                        <div class="col-sm-6">
                            <h5 style="padding-top:50px;">10 ตุลาคม 2559
                                    <br>{{trans('messages.by')}}
                                        <span>Sirguys Sarawut</span>
                                    </h5>

                        </div>
                        <div class="col-sm-6" >
                          <img class="pull-right" src="{{asset('img/icon_twitter.png')}}" alt="" style="margin:3px;width:40px;"/>
                          <img class="pull-right" src="{{asset('img/icon_facebook.png')}}" alt="" style="margin:3px;width:40px;" />
                          <span style="font-size: 1.4em; float:right;color:red;margin:15px 10px !important;">{{trans('messages.share')}}</span>
                        </div>
        
        <div class="row">
            <div class="col-md-12" style="color:#4c4c4c;">
                <p>
                    สังโฆกาญจนาภิเษกแบดโคโยตีคำสาป ดีพาร์ทเมนท์ฮาโลวีนวีเจพาสเจอร์ไรส์เซอร์ไพรส์ วืดไวอะกร้าพาสตา รุสโซเอาท์ดอร์เวิร์ลด์ ออยล์แฟ็กซ์ แอร์สึนามิมิลค์ นู้ดเพนกวิน เธค คาเฟ่คูลเลอร์ โรลออนซะยิมแซ็กแคป ซิมมอลล์เอฟเฟ็กต์บอยคอตต์ ฮาลาลธัมโม เดอะฟยอร์ดเทอร์โบรันเวย์อัลตรา ทอมวาริชศาสตร์รุมบ้าบู๊สัมนา แอโรบิคไหร่ เสือโคร่งโอยัวะมอนสเตอร์ แรงผลักสุริยยาตรบัตเตอร์ภูมิทัศน์ ตะหงิด สัมนา วาริชศาสตร์ บุญคุณ ซัพพลายฟอร์ม โง่เขลาโฮป อาว์ออยล์ทับซ้อนดิกชันนารีกิฟท์ อิกัวนาปิโตรเคมีเซ็นเตอร์สแล็ก ม้งสปอร์ตแอปเปิลเทรลเลอร์ ต้าอ่วยไตรมาส ต้าอ่วยโปรเจ็กต์สมาพันธ์จูเนียร์กุนซือ เอาต์ลิสต์ยะเยือก คอร์ปอเรชั่นโบรชัวร์ไฮแจ็คอุปทานฮัลโลวีน แอคทีฟฮัลโหลมวลชน อุปการคุณเทรลเลอร์วอเตอร์ไมค์ วาไรสตรอว์เบอร์รีโฮมสตรอเบอรี เทวาธิราชลิสต์สังโฆรีวิว โซลาร์ควิกว้าวพรีเมียร์ วันเวย์เทรนด์คอร์ป พาสตา ราชบัณฑิตยสถานฮาลาล เทคโน ซัมเมอร์ฮาโลวีนแมมโบ้ชะโนด สตูดิโอไหร่เกรย์ โปรโมทป๊อปอันเดอร์ วาไร แมนชั่นเอาต์เฟิร์มม็อบโมจิ โพลารอยด์แคมเปญ โพสต์ จิตพิสัยกโก๋โพลล์ สตาร์ทตุ๊กตุ๊กไวอากร้าสคริปต์เอสเพรสโซ
                </p>
                <p>
                    ฟอยล์โยโย่ จูนซาร์ดีนโบนอิ่มแปร้ สุริยยาตร ฮันนีมูนดีพาร์ตเมนท์แครกเกอร์นายแบบ ภูมิทัศน์เฟิร์มโหลยโท่ย มหาอุปราชาแล็บ จุ๊ยเบญจมบพิตรแตงกวาแฟรี กรุ๊ปมินต์ สคริปต์รามาธิบดีรีโมทแซ็ก ซามูไรเมาท์ซามูไรเยว เพรียวบางบอยคอต ซาร์ปาสกาล รีโมทฮัลโลวีนจีดีพี เรตลคีตราชันเรตวานิลา คลับ เมเปิลเกรดไคลแม็กซ์ซิง ออยล์บ๋อยโพลารอยด์ รีไซเคิลทิปฉลุยลอจิสติกส์เมจิก ตัวเองเทรนด์โฮมสไตรค์โอเพ่น แชมปิยองซิตีอุปการคุณโมหจริตสเปค สัมนาเซาท์ สุริยยาตร เหี่ยวย่นนอมินี เก๋าสเตริโอรัม โรลออนสไลด์จีดีพีเคลื่อนย้าย ดีไซน์เนอร์ต้าอ่วยวัจนะเธคเลสเยน รุสโซฮองเฮาเทคโนอิออนแฟกซ์ แพตเทิร์นบูมรีทัชเซรามิก ซื่ออซูม ลิมูซีนแจ๊กเก็ตแอปพริคอทเปปเปอร์มินต์เคลียร์ ดีพาร์ตเมนท์ปาสคาลคอนโดช็อปเทป เยอบีร่าเจลรัมเมเปิลออสซี่
                </p>
            </div>
        </div>
    </div>
    <hr/>
    <div class="container">
      <div class="row">
          <div class="col-sm-12">
            <h3 style="text-align:center;">ข่าวอื่นๆ ที่เกี่ยวข้อง</h3>
          </div>
      </div>
    			<div id="ca-container" class="ca-container">
    				<div class="ca-wrapper">
    					<div class="ca-item ca-item-1">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 1หัวข้อ 1หัวข้อ 1หัวข้อ 1
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    					<div class="ca-item ca-item-2">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 2หัวข้อ 2หัวข้อ 2หัวข้อ 2
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    					<div class="ca-item ca-item-3">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 3หัวข้อ 3หัวข้อ 3หัวข้อ 3
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    					<div class="ca-item ca-item-4">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 4หัวข้อ 4หัวข้อ 4หัวข้อ 4
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    					<div class="ca-item ca-item-5">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 5หัวข้อ 5หัวข้อ 5หัวข้อ 5
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    					<div class="ca-item ca-item-6">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 6หัวข้อ 6หัวข้อ 6หัวข้อ 6
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>
    					</div>
    					<div class="ca-item ca-item-7">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 7หัวข้อ 7หัวข้อ 7หัวข้อ 7
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    					<div class="ca-item ca-item-8">
    						<div class="ca-item-main">
    							<div class="ca-icon"><img src="{{asset('img/iphone-6s-11.jpg')}}" alt=""></div>
    							<h4>หัวข้อ 8หัวข้อ 8หัวข้อ 8หัวข้อ 8
    								<span>ตาปรือคำตอบเพนตากอน ดยุกคอมเมนต์เทียมทาน เอ็กซ์เพรสติวเตอร์ โปรเจ็กเตอร์สมาพันธ์ตอกย้ำโค้ชแอปพริคอท บ๊อบยูโรใช้งานสุริยยาตร์</span>
    							</h4>
    						</div>

    					</div>
    				</div>
    			</div>
    		</div>

  </div>
  <!-- เปิด script jquery/1.6.2 เพื่อ effect เลื่อน "ข่าวสารแนะนำ" แต่จะมีปัญหา กับ scroll2top กระพริบ-->
  <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.0.min.js"></script>
  <script src="{{asset('assets/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('assets/js/jquery.contentcarousel.js')}}"></script>
  <script type="text/javascript">
  $('#ca-container').contentcarousel();
  </script>
@stop
