@extends('layouts.main')
@section('title','video_list')
@section('style')
@stop
@section('content')

    <section id="hero-about"> 
              <div class="container" style="padding-bottom:0;">
                <div class="row">
              @foreach ($videos as $video) 
                  <div class="col-sm-12 vid-container" >
                      <iframe
                          id="vid_frame"
                          src="https://www.youtube.com/embed/{{ $video -> video_id }}?rel=0&amp;controls=0&amp;showinfo=0"
                          frameborder="0" >
                      </iframe>
                  </div>
                  
                  <div class="row">
                      <div class="col-sm-12 video_title stroke">
                          <p style="color:red;text-align:center;">{{ $video -> video_name }}<br>
                            <span class="non_stroke" style="color:#fff;font-size:0.8em;margin-top:0;">ดู 230,500 ครัง - 3 สัปดาห์ที่ผ่านมา...</span>
                          </p>
                          
                      </div>
                  </div>
                  <!-- <div class="col-sm-12">
                      <div class="embed-responsive embed-responsive-16by9">
                          <iframe width="560" height="315" class="embed-responsive-item" src="//www.youtube.com/embed/ePbKGoIGAXY"></iframe>
                      </div>
                  </div> -->
                  @endforeach
                </div>
              </div>
    </section>
                <div class="container">
                  <div class="row">
                          <div id="pager">

                          <div class="col-md-offset-5 col-md-10">
                            <?php for ($i=0; $i < 22 ; $i++) { ?>
                             <span></span>
                            <?php } ?>
                          </div>
                          
                          

                          </div>
<!--                           <hr class="style-seven">
 -->                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                        <h1 style="color: gray;">
                          วิดีโอโฆษณา
                        </h1>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-xs-12 col-md-6">
                        <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                          <div style="cursor:pointer;" class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                            <div class="thumb">
                              <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/_xP3fI7yn5s/0.jpg"></a>
                            </div>
                            <!-- <div class="desc">
                              Anakin Skywalker vs. Obi-Wan Kenobi
                            </div> -->
                            <div class="col-sm-6" style="float:none;margin: 0 auto;">
                                <p style="color:red;text-align:center;line-height:1.5;">Anakin Skywalker vs. Obi-Wan Kenobi<br>
                                  <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครัง - 3 สัปดาห์ที่ผ่านมา...</span>
                                </p>

                            </div>
                          </div>
                        </div>

                      </div>

                      <!-- <div class="col-xs-12 col-md-6">
                        <a href="#" class="thumbnail">
                          <img src="img/iphone-6s-11.jpg" alt="...">
                        </a>
                        <div class="col-sm-6" style="float:none;margin: 0 auto;">
                            <p style="color:red;text-align:center;">โครงการปลูกต้นไม้เฉลิมพระเกียรติ...<br>
                              <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครั้ง - 3 สัปดาห์ที่ผ่านมา...</span>
                            </p>

                        </div>

                      </div> -->
                      <div class="col-xs-12 col-md-6">
                        <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/eYT3ctPuVRw?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                          <div style="cursor:pointer;" class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                            <div class="thumb">
                              <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/eYT3ctPuVRw/0.jpg"></a>
                            </div>
                            <!-- <div class="desc">
                              Anakin Skywalker vs. Obi-Wan Kenobi
                            </div> -->
                            <div class="col-sm-6" style="float:none;margin: 0 auto;">
                                <p style="color:red;text-align:center;line-height:1.5;">Obi-Wan & Anakin vs Count Dooku - Revenge of the Sith<br>
                                  <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครัง - 3 สัปดาห์ที่ผ่านมา...</span>
                                </p>

                            </div>
                          </div>
                        </div>

                      </div>
                  </div>

                  
                    <div class="row" style="padding-bottom:10%;">
                     @foreach($videos as $video)
                        <div class="col-xs-6 col-md-2">
                          <div class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/{{ $video -> video_id }}?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                            <div style="cursor:pointer;" class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                              <div class="thumb">
                                <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/{{ $video -> video_id}}/0.jpg"></a>
                              </div>
                              <div class="desc">
                              <p style="font-size: .9em; color:red;text-align:center;line-height:1;">{{ $video -> video_name }}<br>
                                  <span style="color:#6F6F6F;font-size:0.6em;margin-top:0;">ดู 230,500 ครัง - 3 สัปดาห์ที่ผ่านมา...</span>
                                </p>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      @endforeach
                        <!-- <div class="col-xs-6 col-md-2">
                          <div class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/XdBOlED1zoA?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                            <div style="cursor:pointer;" class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                              <div class="thumb">
                                <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/XdBOlED1zoA/0.jpg"></a>
                              </div>
                              <div class="desc">
                                 Yoda vs. Darth Sidious
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                          <div class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/YBLcxXR1PMw?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                            <div style="cursor:pointer;" class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                              <div class="thumb">
                                <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/YBLcxXR1PMw/0.jpg"></a>
                              </div>
                              <div class="desc">
                                Star Wars Mace Windu vs Darth Sidious
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                          <div class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/tUW7EDJmWUA?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                            <div style="cursor:pointer;" class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                              <div class="thumb">
                                <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/tUW7EDJmWUA/0.jpg"></a>
                              </div>
                              <div class="desc">
                                Star Wars: The Force Awakens Super Trailer
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                          <div class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/OtjqCzV-bSY?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                            <div style="cursor:pointer;" class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                              <div class="thumb">
                                <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/OtjqCzV-bSY/0.jpg"></a>
                              </div>
                              <div class="desc">
                                Star Wars Episode 7: The "New Yoda!" Starkiller Base!
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-2">
                          <div class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/bmP-wGV-1ss?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                            <div style="cursor:pointer;" class="vid-item-list" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?rel=0&amp;controls=0&amp;showinfo=0'">
                              <div class="thumb">
                                <a href="#" class="scroll2thetop"><img src="http://img.youtube.com/vi/bmP-wGV-1ss/0.jpg"></a>
                              </div>
                              <div class="desc">
                                Star Wars Episode 7: BETTER THAN THE PREQUELS!
                              </div>
                            </div>
                          </div>
                        </div> -->
                    </div>
                  
              </div>

@stop
