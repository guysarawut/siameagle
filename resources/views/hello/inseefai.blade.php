@extends('layouts.main')
@section('title','inseefai')
@section('content')
<section id="hero-inseefai">
    <div class="container">
        <div class="row" style="">
            <div class="col-sm-12">
                <div id="" class=""><img class="img-responsive" src="" alt="" style="margin: 100px 0 0 185px!important;max-width:70%;" />
                </div>
                <!-- <div class="col-md-6">
              <div class="about_product" style="width:60%;max-height:70px;transform:translate(-0px,150px);">
                <p style="padding:10px;padding-right:10%;text-align:left;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">{{ trans('messages.dexpanthenol')}}</strong><br> <br> บำรุงประสาทและสมอง</p>
              </div>

            </div>
            <div class="col-md-6">
              <div class="about_product about--to_left" style="width:35%;max-height:70px;transform:translate(-40px,300px);">
                <p style="padding:10px;padding-left:10%;text-align:right;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">{{ trans('messages.fiber')}}</strong><br> <br>ช่วยระบบขับถ่าย</p>
              </div>
            </div>

            <div class="col-md-6">
              <div class="about_product" style="width:35%;max-height:70px;transform:translate(60px,90px);">
                <p style="padding:10px;padding-right:10%;text-align:left;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">{{ trans('messages.vitaminC')}}</strong><br> <br> แก้ร้อนใน</p>
              </div>

            </div>
            <div class="col-md-6">
              <div class="about_product about--to_left" style="transform: translate(35px,5px);;width:75%;" >
                <p style="padding:10px;padding-left:10%;text-align:right;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">{{ trans('messages.Inositol')}}</strong> <br><br> สร้างเซลล์ในกระดูกอ่อน ลดการสะสมของไขมัน</p> 
              </div>
            </div> -->
                <div class="non_animated bottle"><img src="" alt="" />
                </div>
            </div>
            <!-- <div class="col-sm-6">
                <div id="animated-example" class="animated "><img src="" alt="" />
                </div>
                <div class="non_animated bottle"><img src="" alt="" />
                </div>
            </div> -->
        </div>
    </div>
 
    <span><img class="img-responsive can01_title--480" src="img/c1_img_can01_title_480.png" alt=""/> </span>
    <div class="container">
        
    </div>

    <div class="container">

        <div class="row" style="bottom:200px;">
            <div class="col-xs-12 div_480">
                <strong>  เติมความสดชื่น ตื่นจากความอ่อนล้า <br>เพิ่มการเผาผลาญไขมัน และฟื้นฟูในระดับเซลล์ <br>ใส่คำบรรยายสรรพคุณสินค้านี้ต่อไปเรื่อยๆ<br>
                 บำรุงสายตา ช่วยให้สดชื่น ตื่นตัวอยู่เสมอ <br>บำรุงระบบประสาทและสมอง และระบบขับถ่าย<br>ดื่มกี่ขวดก็ได้ไม่จำกัด</strong>
            </div>
        </div>
        <!-- <div class="row" style="top:200px;">
            <center><img class="img-responsive" src="{{asset('img/251ml.png')}}" alt="" style="margin-bottom:20px;"/></center>
        </div> -->

    </div>

</section>

<section class="inseefai">
    <div class="container" style="text-align:center;">
        <div class="row">
            <img src="{{asset('img/inseefai_img.png')}}" alt="" style="margin-bottom: 10%;" />
            
        </div>
        <div class="row" style="padding:0 20%;">
            <div class="col-sm-4">
              <img src="{{asset('img/img_indeefai_can.png')}}" alt="" style="width:70px;height:180px;" />
            </div>
            
            <div class="col-sm-4">
              <p style="background-color:#e91c28;color:#fff;margin:10px 15px;border-radius:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
                {{ trans('messages.ingredient')}}
              </p>
            <div class="" style="margin:0 -40px;">
              {{trans('messages.b3')}} 0.012 % / {{trans('messages.b5')}} 0.004 % <br>
              {{trans('messages.b6')}} 0.004 % / {{trans('messages.b12')}} 0.0012 % <br>
              {{trans('messages.fiber')}} 1% / {{trans('messages.taurine')}}  0.532 % <br>
              {{trans('messages.Inositol')}} 0.532 % / {{trans('messages.sugar')}} 8 % <br>
              {{ trans('messages.artificial')}}
            </div>
            </div>
            <div class="col-sm-4">
                <img src="{{asset('img/img_indeefai_pel.png')}}" alt="" style="width:70px;height:180px;" />
            </div>
        </div>
    </div>
</section>
@stop
