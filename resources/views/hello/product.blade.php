@extends('layouts.main')
@section('title','Product')
@section('style')
<style type="text/css">
    .product_fix {
        padding:150px;
    }
@-moz-document url-prefix() {
    .product_fix {
        margin-top:300px;
    }
}
</style> 
@stop
@section('content')
<section id="hero">
    <div class="container">
        <div class="row product__content--max_height">

          <div class="row product__content">
            <div class="col-md-6">
              <div class="about_product" style="width:60%;max-height:70px;transform:translate(60px,110px);">
                <p style="padding:10px;padding-right:10%;text-align:left;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">เด็กซ์แพนธินอล</strong><br> <br> บำรุงประสาทและสมอง</p>
              </div>

            </div>
            <div class="col-md-6">
              <div class="about_product about--to_left" style="width:35%;max-height:70px;transform:translate(-120px,50px);">
                <p style="padding:10px;padding-left:10%;text-align:right;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">ไฟเบอร์</strong><br> <br>ช่วยระบบขับถ่าย</p>
              </div>
            </div>

            <div class="col-md-6">
              <div class="about_product" style="width:35%;max-height:70px;transform:translate(125px,90px);">
                <p style="padding:10px;padding-right:10%;text-align:left;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">วิตามินซี</strong><br> <br> แก้ร้อนใน</p>
              </div>

            </div>
            <div class="col-md-6">
              <div class="about_product about--to_left" style="transform: translateX(-5%);width:75%;" >
                <p style="padding:10px;padding-left:10%;text-align:right;line-height:100%;color:#fff;"><strong style="font-size:1.5em;">อินโนซิทอล</strong> <br><br> สร้างเซลล์ในกระดูกอ่อน ลดการสะสมของไขมัน</p> 
              </div>
            </div>

          </div>
          <img class="product_benefit--img--480" src="img/c3_img_benefit_indicator.png" alt="" style="float:left;z-index:1;position:relative;width:50%;" />
          <div class="row b6_480">
              <div class="col-xs-12" >
              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_01.png" alt="" /><strong><h4 class="benefit--h4">ซูโครส</h4><h5 class="benefit--h5">เติมความสดชื่น ตื่นจากความอ่อนล้า</h5></strong>

              </div>
              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_02.png" alt="" /><strong><h4 class="benefit--h4">วิตามิน B3</h4><h5 class="benefit--h5">ลดการเกิดความดันโลหิตสูง โรคไมเกรน </h5></strong>
              </div>

              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_03.png" alt="" /><strong><h4 class="benefit--h4">วิตามิน B6</h4><h5 class="benefit--h5">ช่วยสังเคราะห์เม็ดเลือดแดง และการดูดซึม</h5></strong>
              </div>
              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_04.png" alt="" /><strong><h4 class="benefit--h4">วิตามิน B12</h4><h5 class="benefit--h5">มีความสำคัญต่อเซลล์ ทุกชนิด </h5></strong>
              </div>

              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_05.png" alt="" /> <br><strong><h4 class="benefit--h4">ทอรีน</h4><h5 class="benefit--h5">ดีต่อดวงตา </h5></strong>
              </div>
              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_06.png" alt="" /><strong><h4 class="benefit--h4">คาเฟอีน</h4><h5 class="benefit--h5">ร่างกายสดชื่นตื่นตัวอยู่เสมอ </h5></strong>
              </div>

              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_07.png" alt="" /> <br><strong><h4 class="benefit--h4">อินโนซิทอล</h4><h5 class="benefit--h5">สร้างเซลล์ในกระดูกอ่อน
                  ลดการสะสมของไขมัน</strong></h5>
              </div>
              <div class="col-xs-12">
                <img style="width:90px;height:90px;" src="" alt="" /><br><strong><h4 class="benefit--h4">เด็กซ์แพนธินอล</h4><h5 class="benefit--h5">บำรุงประสาทและสมอง </h5></strong>
              </div>
              </div>
          </div>

            <div class="col-sm-12">
                <div id="animated-example" class="animated"><img  src="" alt="" />
                </div>
                <img class="product_fix" style="" src="img/img_benefit_product_label.png" alt="" />
            </div>

        </div>
    </div>
</section>
@stop
