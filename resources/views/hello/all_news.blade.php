@extends('layouts.main')
@section('title','News')
@section('style')
@stop
@section('content')
<div class="sections">
    <section id="hero-about"> 
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     
                    
                    <div class="image_news">
                    <img class="img-responsive" src="{{ asset('img/sci_18.jpg')}}" alt="" style="height:490px;width:830px; margin:0 auto;" />
                        <h2>กิจกรรมรับน้องใหม่<br />สาขาวิชาชีววิทยา คณะวิทยาศาสตร์และเทคโนโลยี</h2> <br/>
                        <h4>การเรียนรู้ภาษาไทยทั้งในด้านพูดและเขียน สามารถนำไปสู้รายได้ที่สูงขึ้นและมีแนวโน้มสูงมากขึ้นในด้านการประกอบอาชีพของนักศึกษามหาวิทยาลัยกัมพูชา</h4>
                    </div>
                    
                    <!-- <div class="">
                    <img class="img-responsive" src="" alt="" style="width:80%; margin:0 auto;" />
                        
                    </div> -->
                    
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="padding-bottom:0;">
                        
        <div class="row">
<!--             <div class="col-md-6" style="color:#4c4c4c;">
 -->                

                <div class="row">
                <div class="col-sm-6">
                    <h1>{{ trans('messages.worker')}}</h1>
                </div>
                <div class="col-sm-6">
                    <h1>{{ trans('messages.sports')}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/shutterstock_96941162.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                    
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border" >Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/Surfing.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border">Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row"><hr></div>
            <!-- <a href="content_list.php"><h4 style="color:#4c4c4c;text-align:right;padding:10px;top:0;top:0;">ดูเพิ่มเติม</h4></a> -->
            <div class="row">
                <div class="col-sm-6">
                    <h1>{{ trans('messages.agriculture')}}</h1>
                </div>
                <div class="col-sm-6">
                    <h1>{{ trans('messages.education')}}</h1>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/B4572.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border">Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/10479682_4714297791833_721074049793301617_o.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border">Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
<!--             </div>
 -->        </div>
    </div>
    <hr/>
    

  </div>
  <!-- เปิด script jquery/1.6.2 เพื่อ effect เลื่อน "ข่าวสารแนะนำ" แต่จะมีปัญหา กับ scroll2top กระพริบ-->
  <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.0.min.js"></script>
  <script src="{{asset('assets/js/jquery.easing.1.3.js')}}"></script>
  
@stop
