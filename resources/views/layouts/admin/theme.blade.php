<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('page_title')</title>
    <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="{{ url('font-awesome/css/font-awesome.min.css') }}" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="{{ url('style/backend/style.css') }}" media="screen" title="no title" charset="utf-8">
    <link rel="shortcut icon" href="{{ url('images/favicon/ico_favicon.ico') }}" type="image/x-icon">
    @section('custom-css') @show
    @yield('custom-css')
  </head>
  <body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('admin') }}">Brand</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
          <ul class="nav navbar-nav">
          @if ( Auth::user()->role ==1)
            <li class="{{ Request::segment(2) == 'user' ? 'active' : '' }}"><a href="{{ url('admin/user/show/1') }}">Admin</a></li>
            @endif
            <!-- <li><a href="{{ url('admin/contact/') }}">Contact</a></li> -->
            <li class="{{ Request::segment(2) == 'convenient' ? 'active' : '' }}"><a href="{{ url('admin/convenient/') }}">News & Event</a></li>
            <!-- <li><a href="{{ url('admin/standard/') }}">Standard</a></li> -->
            <li class="{{ Request::segment(2) == 'video' ? 'active' : '' }}"><a href="{{ url('admin/video/') }}">Video</a></li>
            <li class="{{ Request::segment(2) == 'logs' ? 'active' : '' }}"><a href="{{ url('admin/logs/') }}">Logs</a></li>
            
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('admin/logout') }}">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      @yield('content')
    </div>

    <script type="text/javascript" src="{{ url('jquery/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('bootstrap/js/bootstrap.min.js') }}"></script>
    @section('custom-js') @show
  </body>
</html>
