<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class Convenient extends Model implements LogsActivityInterface
{
  use LogsActivity;
    protected $fillable = [
      'convenient_name_th',
      'convenient_detail_th',
      'convenient_name_en',
      'convenient_detail_en',
      'convenient_name_cn',
      'convenient_detail_cn',
      'convenient_image_th',
      'convenient_image_en',
      'convenient_image_cn'
    ];
/**
     * Returns translated data 
     * @param  object           $data       recrive object
     * @return object           convenient_name, convenient_detail, convenient_image for user's locale
 */
  public static function tran($data)
  {
    $locale = \App::getLocale();
    foreach ($data as $key => $value) 
    {
        if($locale=='en')
        {
            $value->convenient_name = $value->convenient_name_en;
            $value->convenient_detail = $value->convenient_detail_en;
            $value->convenient_image = $value->convenient_image_en;
        }
        elseif($locale=='th')
        {
            $value->convenient_name = $value->convenient_name_th;
            $value->convenient_detail = $value->convenient_detail_th;
            $value->convenient_image = $value->convenient_image_th;
        }
        elseif($locale=='cn')
        {
            $value->convenient_name = $value->convenient_name_cn;
            $value->convenient_detail = $value->convenient_detail_cn;
            $value->convenient_image = $value->convenient_image_cn;
        }
        unset($value['convenient_name_en']);
        unset($value['convenient_name_th']);
        unset($value['convenient_name_cn']);
        unset($value['convenient_detail_en']);
        unset($value['convenient_detail_th']);
        unset($value['convenient_detail_cn']);
        unset($value['convenient_image_en']);
        unset($value['convenient_image_th']);
        unset($value['convenient_image_cn']);
    }
    return $data;
  }

  public function getActivityDescriptionForEvent($eventName)
  {
    if ($eventName == 'created')
    {
        return 'News&Event "' . $this->convenient_name_th . '" was created';
    }

    if ($eventName == 'updated')
    {
        return 'News&Event "' . $this->convenient_name_th . '" was updated';
    }

    if ($eventName == 'deleted')
    {
        return 'News&Event "' . $this->convenient_name_th . '" was deleted';
    }

    return '';
  }
}
