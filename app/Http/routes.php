<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Back End */
Route::group(['middleware' => 'auth'], function () {
  Route::controller('admin/video', 'Admin\Video\VideoController');
  Route::controller('admin/standard', 'Admin\Standard\StandardController');
  Route::controller('admin/convenient', 'Admin\Convenient\ConvenientController');
  Route::controller('admin/contact', 'Admin\Contact\ContactController');
  Route::controller('admin/user', 'Admin\User\UserController');
  Route::controller('admin/logs', 'Admin\logs\LogsController');
  Route::controller('admin', 'Admin\AdminController');
});

// Localization
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
  Route::get('/', 'DevController@home');
  Route::get('/test', 'DevController@news');

  /* Front End */
  Route::get('home', 'HelloController@index');
  Route::get('inseefai', 'HelloController@inseefai');
  Route::get('news', 'HelloController@news');
  Route::get('product', 'HelloController@product');
  // Route::get('who', 'HelloController@who');
  Route::get('about', 'HelloController@about');
  Route::get('contact', 'HelloController@contact');
  Route::get('video_list', 'HelloController@video_list' );
  Route::get('users', function()
  {
      return View::make('hello.news');
  });

  Route::get('content_list' , function()
  {
      return View::make('hello.content_list');
  });

  Route::get('all_news', function()
  {
      return View::make('hello.all_news');
  });
});
// End Localization

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');