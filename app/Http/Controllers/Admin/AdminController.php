<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Activity;

class AdminController extends Controller
{
    public function getIndex() {
      return view('admin.index');
    }

    public function getLogout() {
      if (Auth::check()) {
        Activity::log(Auth::user()->email.' logged out');
        Auth::logout();
        return redirect('auth/login');
      }
      else {
        return back();
      }
    }
}
