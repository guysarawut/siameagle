<?php

namespace App\Http\Controllers\Admin\Convenient;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Convenient;

use Validator;
use File; 

class ConvenientController extends Controller
{
    public function getIndex() {
      $convenients = Convenient::all();
      return view('admin.convenient.index', compact('convenients'));
    }

    public function getCreate() {

      return view('admin.convenient.create');
    }

    public function postCreate(Request $request) {
      // dd($request->convenient_name_th);
      // Check request input
      $validator = Validator::make($request->all(), [
            'convenient_name_th'   => 'required',
            'convenient_detail_th' => 'required',
            'convenient_name_en'   => 'required',
            'convenient_detail_en' => 'required',
            'convenient_name_cn'   => 'required',
            'convenient_detail_cn' => 'required',
            'category' => 'required',
        ]);
      // Check validator
      if ($validator->fails()) {
          return back()
                  ->withErrors($validator)
                  ->withInput();
      }

      // Create convenient
      $convenient = Convenient::create($request->all());
      // Manage Image

      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $filename = date('his')."Convenient".$image->getClientOriginalName();
        $public_path = "images/upload";
        $destination = base_path()."/public/".$public_path;
        $request->file('image')->move($destination, $filename);
        Convenient::find($convenient->id)->update([
          'convenient_image_th' => $filename
        ]);
        // echo $filename . "<br>";
      }

      if ($request->hasFile('image_en')) {
        $image_en = $request->file('image_en');
        $filename_en = date('his')."Convenient".$image_en->getClientOriginalName();
        $public_path = "images/upload";
        $destination = base_path()."/public/".$public_path;
        $request->file('image_en')->move($destination, $filename_en);
        Convenient::find($convenient->id)->update([
          'convenient_image_en' => $filename_en
        ]);
        // echo $filename_en . "<br>";
      }

      if ($request->hasFile('image_cn')) {
        $image_cn = $request->file('image_cn');
        $filename_cn = date('his')."Convenient".$image_cn->getClientOriginalName();
        $public_path = "images/upload";
        $destination = base_path()."/public/".$public_path;
        $request->file('image_cn')->move($destination, $filename_cn);
        Convenient::find($convenient->id)->update([
          'convenient_image_cn' => $filename_cn
        ]);
        // echo $filename_cn;
      }

      return redirect('admin/convenient');
    }

    public function getUpdate($id) {
      $convenient = Convenient::find($id);
      return view('admin.convenient.edit', compact('convenient'));
    }

    public function postUpdate(Request $request) {
      // Check request input
      $validator = Validator::make($request->all(), [
            'convenient_name_th'   => 'required',
            'convenient_detail_th' => 'required',
            'convenient_name_en'   => 'required',
            'convenient_detail_en' => 'required',
            'convenient_name_cn'   => 'required',
            'convenient_detail_cn' => 'required',

        ]);
      // Check validator
      if ($validator->fails()) {
          return back()
                  ->withErrors($validator)
                  ->withInput();
      }
      // Convenient id
      $convenient_id = $request->id;
      // Create Model convenient and menage image
      $convenient = Convenient::find($request->id);
      if ($request->hasFile('image')) {
        if( $convenient->convenient_image_th != "" ) {
          $public_path = "images/upload";
          File::Delete($public_path."/".$convenient->convenient_image_th);
          $image = $request->file('image');
          $filename = date('his')."Convenient".$image->getClientOriginalName();
          $destination = base_path()."/public/".$public_path;
          $request->file('image')->move($destination, $filename);
        }
        else {
          $image = $request->file('image');
          $filename = date('his')."Convenient".$image->getClientOriginalName();
          $public_path = "images/upload";
          $destination = base_path()."/public/".$public_path;
          $request->file('image')->move($destination, $filename);
        }
      }
      else {
        $filename = $convenient->convenient_image_th;
      }
      // Save convenient
      $convenient->convenient_name_th = $request->convenient_name_th;
      $convenient->convenient_detail_th = $request->convenient_detail_th;
      $convenient->convenient_name_en = $request->convenient_name_en;
      $convenient->convenient_detail_en = $request->convenient_detail_en;
      $convenient->convenient_name_cn = $request->convenient_name_cn;
      $convenient->convenient_detail_cn = $request->convenient_detail_cn;
      $convenient->convenient_image_th = $filename;
      $convenient->save();

      return redirect('admin/convenient');
    }
    public function getDelete($id) {
      // Convenient::where('id', $id)->delete();
      $data = Convenient::find($id);
      $data->delete();
      return back();
    }
}


