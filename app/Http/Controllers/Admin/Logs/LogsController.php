<?php

namespace App\Http\Controllers\Admin\Logs;

use Illuminate\Http\Request;
use App\Admin;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;

class LogsController extends Controller
{
   public function getIndex() {
   	$logs = Activity::with('user')->latest()->paginate(20);
    return view('admin.logs.index',compact('logs'));
    }
}
