<?php

namespace App\Http\Controllers\Admin\Contact;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contact;

use Validator;
use File;

class ContactController extends Controller
{
    public function getIndex() {
      $contacts = Contact::all();
      return view('admin.contact.index', compact('contacts'));
    }

    public function getCreate() {
      return view('admin.contact.create');
    }

    public function postCreate(Request $request) {
      // Check request input
      $validator = Validator::make($request->all(), [
            'company_name_th'      => 'required',
            'company_name_en'      => 'required',
            'company_address_th'   => 'required',
            'company_address_en'   => 'required',
            'company_tel'          => 'required',
            'company_fax'          => 'required',
            'company_email'        => 'required',
            'company_fooice_house' => 'required'
        ]);
      // Check validator
      if ($validator->fails()) {
          return back()
                  ->withErrors($validator)
                  ->withInput();
      }
      // Manage image Google Map
      if ($request->hasFile('company_google_map')) {
        $image = $request->file('company_google_map');
        $filename_google_map = date('his')."Contact".$image->getClientOriginalName();
        $public_path = "images/upload";
        $destination = base_path()."/public/".$public_path;
        $request->file('company_google_map')->move($destination, $filename_google_map);
      }
      else {
        $filename_google_map = "";
      }
      // Manage image Image Map
      if ($request->hasFile('company_image_map')) {
        $image = $request->file('company_image_map');
        $filename_image_map = date('his')."Contact".$image->getClientOriginalName();
        $public_path = "images/upload";
        $destination = base_path()."/public/".$public_path;
        $request->file('company_image_map')->move($destination, $filename_image_map);
      }
      else {
        $filename_image_map = "";
      }
      // Save Data
      Contact::create([
        'company_name_th'      => $request->company_name_th,
        'company_name_en'      => $request->company_name_en,
        'company_address_th'   => $request->company_address_th,
        'company_address_en'   => $request->company_address_en,
        'company_tel'          => $request->company_tel,
        'company_fax'          => $request->company_fax,
        'company_email'        => $request->company_email,
        'company_fooice_house' => $request->company_fooice_house,
        'company_google_map'   => $filename_google_map,
        'company_image_map'    => $filename_image_map
      ]);


      return redirect('admin/contact');
    }

    public function getUpdate($id) {
      $contact = Contact::find($id);
      return view('admin.contact.edit', compact('contact'));
    }

    public function postUpdate(Request $request) {
      // Check request input
      $validator = Validator::make($request->all(), [
            'company_name_th'      => 'required',
            'company_name_en'      => 'required',
            'company_address_th'   => 'required',
            'company_address_en'   => 'required',
            'company_tel'          => 'required',
            'company_fax'          => 'required',
            'company_email'        => 'required',
            'company_fooice_house' => 'required'
        ]);
      // Check validator
      if ($validator->fails()) {
          return back()
                  ->withErrors($validator)
                  ->withInput();
      }
      // Create Model Contact
      $contact = Contact::find($request->id);
      // Manage image Google Map
      if ($request->hasFile('company_google_map')) {
        if( $contact->company_google_map != "" ) {
          $public_path = "images/upload";
          File::Delete($public_path."/".$contact->company_google_map);
          $image = $request->file('company_google_map');
          $filename_google_map = date('his')."Contact".$image->getClientOriginalName();
          $destination = base_path()."/public/".$public_path;
          $request->file('company_google_map')->move($destination, $filename_google_map);
        }
        else {
          $image = $request->file('company_google_map');
          $filename_google_map = date('his')."Contact".$image->getClientOriginalName();
          $public_path = "images/upload";
          $destination = base_path()."/public/".$public_path;
          $request->file('company_google_map')->move($destination, $filename_google_map);
        }
      }
      else {
        $filename_google_map = $contact->company_google_map;
      }

    // Manage image Image Map
    if ($request->hasFile('company_image_map')) {
      if( $contact->company_image_map != "" ) {
        $public_path = "images/upload";
        File::Delete($public_path."/".$contact->company_image_map);
        $image = $request->file('company_image_map');
        $filename_image_map = date('his')."Contact".$image->getClientOriginalName();
        $destination = base_path()."/public/".$public_path;
        $request->file('company_image_map')->move($destination, $filename_image_map);
      }
      else {
        $image = $request->file('company_image_map');
        $filename_image_map = date('his')."Contact".$image->getClientOriginalName();
        $public_path = "images/upload";
        $destination = base_path()."/public/".$public_path;
        $request->file('company_image_map')->move($destination, $filename_image_map);
      }
    }
    else {
      $filename_image_map = $contact->company_image_map;
    }
    // Save Data
    $contact->Update([
      'company_name_th'      => $request->company_name_th,
      'company_name_en'      => $request->company_name_en,
      'company_address_th'   => $request->company_address_th,
      'company_address_en'   => $request->company_address_en,
      'company_tel'          => $request->company_tel,
      'company_fax'          => $request->company_fax,
      'company_email'        => $request->company_email,
      'company_fooice_house' => $request->company_fooice_house,
      'company_google_map'   => $filename_google_map,
      'company_image_map'    => $filename_image_map
    ]);

    return back();
  }
}
