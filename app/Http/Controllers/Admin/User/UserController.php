<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use Validator;
use App\Admin;

use Hash;
use Activity;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getShow($type) {
      $members = Admin::where('role', '=', $type)->get();
      return view('admin.user.index', compact('members'));
    }

    public function getCreate() {
      return view('admin.user.create');
    }

    public function postCreate(Request $request) {
      $validator = Validator::make($request->all(), [
          'email'       => 'required',
          'password'    => 'required',
          're_password' => 'required',
          'firstname'   => 'required',
          'lastname'    => 'required',
          'role'        => 'required'
      ]);

      if( $validator->fails() ) {
        return back()
              ->withErrors($validator)
              ->withInput();
      }

      if( $request->password != $request->re_password ) {
        return back()
              ->with('message', 'Please enter the same password')
              ->withInput();
      }

      Admin::create($request->all());
      return redirect("admin/user/show/$request->role");
    }

    public function getEdit($id) {
      $member = Admin::find($id);
      return view('admin.user.edit', compact('member'));
    }

    public function postEdit($id, Request $request) {
      $validator = Validator::make($request->all(), [
          'email'       => 'required',
          'firstname'   => 'required',
          'lastname'    => 'required',
          'role'        => 'required'
      ]);

      if( $validator->fails() ) {
        return back()
              ->withErrors($validator);
      }

      if( $request->password != '' ) {
        if( $request->password != $request->re_password ) {
          return back()
                 ->with('message', 'Please enter the same password');
        }
        else {
          Admin::find($id)->update([
            'firstname' => $request->firstname,
            'lastname'  => $request->lastname,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'role'      => $request->role
          ]);
        }
      }
      else {
        Admin::find($id)->update([
          'firstname' => $request->firstname,
          'lastname'  => $request->lastname,
          'email'     => $request->email,
          'role'      => $request->role
        ]);
      }
      Activity::log($request->input('email').' was updated');
      return redirect("admin/user/show/$request->role");
    }

    public function getDelete($id) {
      $user = Admin::find($id);
      $user->delete();
      return back();
    }
}
