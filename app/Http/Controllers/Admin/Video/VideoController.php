<?php

namespace App\Http\Controllers\Admin\Video;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Video;

use Validator;
use File;

class VideoController extends Controller
{
    public function getIndex(){
      $videos = Video::all();
        return view('admin.video.index',compact('videos'));

    }
    public function getCreate(){
        return view ('admin.video.create');
    }
    public function postCreate(Request $request) {
      // dd($request->all());

      // Check request input
      $validator = Validator::make($request->all(), [
            'video_name_th'      => 'required',
            'video_name_en'      => 'required',
            'video_name_cn'      => 'required',
            'url_th'             => 'required'
        ]);
      // Check validator
      if ($validator->fails()) {
          return back()
                  ->withErrors($validator)
                  ->withInput();
      }
 
 // NEW
      $url_th = $request->url_th;
      $url_en = $request->url_en;
      $url_cn = $request->url_cn;
      parse_str( parse_url( $url_th, PHP_URL_QUERY ), $my_array_of_varsTH );
      parse_str( parse_url( $url_en, PHP_URL_QUERY ), $my_array_of_varsEN );
      parse_str( parse_url( $url_cn, PHP_URL_QUERY ), $my_array_of_varsCN );
      // dd ($my_array_of_varsTH['v']);
 // NEW

      // Save Data
      Video::create([
        'video_name_th'      => $request->video_name_th,
        'video_name_en'      => $request->video_name_en,
        'video_name_cn'      => $request->video_name_cn,
        'url_th'             => $request->url_th,
        'url_en'             => $request->url_en,
        'url_cn'             => $request->url_cn,
        'video_idTH'         => $my_array_of_varsTH['v'],
        'video_idEN'         => $my_array_of_varsEN['v'],
        'video_idCN'         => $my_array_of_varsCN['v'],
      ]);

      return redirect('admin/video');
    }

    public function getEdit($id) {
      $videos = Video::find($id);
      // dd ($videos);
      return view('admin.video.edit', compact('videos'));
    }

    public function postEdit(Request $request) {
      // Check request input
      $validator = Validator::make($request->all(), [
            'video_name_th'        => 'required',
            'video_name_en'        => 'required',
            'video_name_cn'        => 'required',
            'url_th'               => 'required'
        ]);
      // Check validator
      if ($validator->fails()) {
          return back()
                  ->withErrors($validator)
                  ->withInput();
      }

      $videos = Video::find($request->id);
      // Videos id
      $video_id = $request->id;
      // dd ($videos);
      // Save videos
      $videos->video_name_th = $request->video_name_th;
      $videos->video_name_en = $request->video_name_en;
      $videos->video_name_cn = $request->video_name_cn;
      $videos->url_th        = $request->url_th;
      $videos->url_en        = $request->url_en;
      $videos->url_cn        = $request->url_cn;
      $videos->save();

      return redirect('admin/video');
    }

    public function getDelete($id) {
      $vdo = Video::find($id);
      $vdo->delete();
      return back();
    }
}
