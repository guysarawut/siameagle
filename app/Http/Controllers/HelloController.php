<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Convenient;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;

class HelloController extends Controller
{

    public function index()
    {

        $video = new Video();
        $videos = $video::get();
        $videos = $video->tran($videos);

        $convenient = new Convenient();
        $news = Convenient::get();
        $news = Convenient::tran($news);
        
        
        
        return view('hello.index',compact('videos','news'));
    }

    public function video_list(){
        $video = new Video();
        $videos = $video::get();
        $videos = $video->tran($videos);

        return view('hello.video_list',compact('videos'));
    }

    public function inseefai ()
    {
        return view('hello.inseefai');
    }
    public function news()
    {
        return view('hello.news');
    }
    public function product ()
    {
        return view('hello.product');
    }
    public function who()
    {
        return view('hello.who');
    }
    public function about ()
    {
        return view('hello.about');
    }
    public function contact ()
    {
        return view('hello.contact');
    }
}
