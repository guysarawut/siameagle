<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class Admin extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    LogsActivityInterface
{

  use Authenticatable, Authorizable, CanResetPassword, LogsActivity;

  /**
   * The database table used by the model.
   *
   * @var string
   */

  protected $table = 'admins';

  protected $fillable = ['firstname', 'lastname', 'email', 'password', 'role', 'image'];

  public function setPasswordAttribute($value) {
    $this->attributes['password'] = Hash::make($value);
  }
  
  public function getActivityDescriptionForEvent($eventName)
  {
    if ($eventName == 'created')
    {
        return 'User  "' . $this->email. '" was created';
    }

    if ($eventName == 'deleted')
    {
        return 'User "' . $this->email. '" was deleted';
    }

    return '';
  }
}
