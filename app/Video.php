<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class Video extends Model implements LogsActivityInterface
{
  use LogsActivity;
    protected $fillable = [
      'video_name_th',
      'video_name_en',
      'video_name_cn',
      'url_th',
      'url_en',
      'url_cn',
      'video_idTH',
      'video_idEN',
      'video_idCN'
    ];

  public function tran($data)
  {
    $locale = \App::getLocale();
    foreach ($data as $key => $value) 
    {
        if($locale=='en')
        {
            $value->video_name = $value->video_name_en;
            $value->url = $value->url_en;
            $value->video_id = $value->video_idEN;
        }
        elseif($locale=='th')
        {
            $value->video_name = $value->video_name_th;
            $value->url = $value->url_th;
            $value->video_id = $value->video_idTH;
        }
        elseif($locale=='cn')
        {
            $value->video_name = $value->video_name_cn;
            $value->url = $value->url_cn;
            $value->video_id = $value->video_idCN;
        }
        unset($value['video_name_en']);
        unset($value['video_name_th']);
        unset($value['video_name_cn']);
        unset($value['url_en']);
        unset($value['url_th']);
        unset($value['url_cn']);
        unset($value['video_idEN']);
        unset($value['video_idTH']);
        unset($value['video_idCN']);
    }
    return $data;
    } 
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return 'Video "' . $this->video_name_th . '" was created';
        }

        if ($eventName == 'updated')
        {
            return 'Video "' . $this->video_name_th . '" was updated';
        }

        if ($eventName == 'deleted')
        {
            return 'Video "' . $this->video_name_th . '" was deleted';
        }

        return '';
    }
}
