<?php

use Illuminate\Database\Seeder;

class FirstUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'firstname' => 'test',
            'lastname' => 'test',
            'email' => 'test@test.com',
            'role' => 1,
            'password' => bcrypt('123456'),
        ]);
    }
}
