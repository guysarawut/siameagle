<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageTable extends Migration
{
    public function up()
    {
        Schema::table('convenients', function (Blueprint $table) {
            $table->string('convenient_image_th');
            $table->string('convenient_image_en');
            $table->string('convenient_image_cn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convenients', function (Blueprint $table) {
            //
        });
    }//
    
}
