<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video_name_th');
            $table->string('video_name_en');
            $table->string('video_name_cn');
            $table->string('url_th');
            $table->string('url_en');
            $table->string('url_cn');
            $table->string('video_idTH');
            $table->string('video_idEN');
            $table->string('video_idCN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
